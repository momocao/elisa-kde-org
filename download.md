---
layout: page
title: Download
sorted: 3

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        !SITE_TITLE! is already available on majority of Linux distriutions. It
        can be installed directly from the <a href="appstream://org.kde.elisa.desktop">Discover</a>,<a href="appstream://org.kde.elisa.desktop"> GNOME Software</a> or your distribution's <a href="appstream://org.kde.elisa.desktop">software store</a>.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        !SITE_TITLE! is released regularily as part of KDE Applications. You can 
        find !SITE_TITLE! latest stable realease among the 
        <a href="https://download.kde.org/stable/applications">tarballs from
        the latest KDE Applications release</a>.

        If you want to build !SITE_TITLE! from sources, we recommend checking our
        <a href="get-involved.html">Getting Involved</a> page which contains
        links to full guide how to compile !SITE_TITLE!.
  - name: Flatpak
    icon: /assets/img/flatpak.png
    description: >
        <b>Install flatpak using :</b><br>
        <i>
         flatpak remote-add --if-not-exists flathub <a href="https://flathub.org/repo/flathub.flatpakrepo">
         https://flathub.org/repo/flathub.flatpakrepo</a></i><br>
        <b>Install Elisa :</b><br>
        <i>flatpak install flathub org.kde.elisa</i>
  - name: Windows
    icon: /assets/img/windows.png
    description: >
        <b>Microsoft store: </b><br>
        <i><a href="https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl">
        https://www.microsoft.com/en-us/p/elisa/9pb5md7zh8tl</a></i><br>
        <b>Win32 executable :</b><br>
        <i><a href="https://binary-factory.kde.org/view/Windows%2064-bit/job/Elisa_Release_win64/">https://binary-factory.kde.org/view/Windows 64-bit/job/Elisa_Release_win64/</a></i>

    

---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100" style="text-align: center">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th style="padding: 10px">{{ source.name }}</th>
    </tr>
    <tr>
        <td style="padding: 10px">{{ source.description | replace: '!SITE_TITLE!', site.title | replace: '!SITE_GIT!', site.git}}</td>
    </tr>
    
{% endfor %}
</table>
